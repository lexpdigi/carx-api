/**
 * Service com funções de CRUD de Produtos
 * 
 */



import {carroDb} from '../../models/carro';
const CarroUtil  = require('../../utils/car.utils')
import {ICreateCarroRequestDto } from './dto/create-carro.dto';
import { IUpdateCarroRequestDto } from './dto/update-carro.dto';

export default class ProdutoService {
  async create(createProdutoDto: ICreateCarroRequestDto) {
    return await carroDb.create(createProdutoDto)
  }

  async findAll() {
    return CarroUtil.formatObject(await carroDb.findAll());
  }

  async findOne(id: number) {
    return  CarroUtil.formatObject( await carroDb.findOne({ where: { id: id} }));
  }

  async update(id: number, updateProdutoDto: IUpdateCarroRequestDto) {
    return await   carroDb.update(updateProdutoDto,{where:{id:id}})
  }

 async delete(id: number) {
   return await carroDb.destroy({
     where:{id:id}
   });
  }
}