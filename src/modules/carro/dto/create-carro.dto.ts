
/**
 * Interface de criação de Carro
 * 
 */

export interface ICreateCarroRequestDto   {
    placa:string;
    chassi:string;
    marca:string;
    modelo:string;
    renavam:string;
    ano:number;
}