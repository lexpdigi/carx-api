
/**
 * Interface de Atualização de Carro
 * 
 */

import { ICreateCarroRequestDto } from "./create-carro.dto";

export interface IUpdateCarroRequestDto extends ICreateCarroRequestDto {
      id:string;
}