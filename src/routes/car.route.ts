/**
 * Rotas com metódos a ser utilizados na rota gameplay
 * Os Metódos permitidos sao GET,POST,PUT,DELETE
 */

 const express = require('express').Router();
import CarService from '../modules/carro/carro.service'
import CarController from '../controllers/car' 
 const router = express;

  const _carConroller = new CarController(new CarService());
 
 router.get('/find/:id',async(req,res)=>{
   return await _carConroller.findOne(req,res);
});
 router.get('/teste',async(req,res)=>{
    return await _carConroller.teste(req,res);
 });
 
 router.post('/create',async(req,res)=>{
    return await _carConroller.create(req,res);
 });
 
 router.get('/all',async(req,res)=>{
   return await _carConroller.read(req,res);
});

 router.put('/:id',async(req,res)=>{
     return await _carConroller.update(req,res);
  });
 
 router.delete('/:id',async(req,res)=>{
    return await _carConroller.delete(req,res);
 });



 module.exports = router;