const chai = require('chai');
const assert = chai.assert;
import supertest from 'supertest';
const connection = require('../../database/index'); 


import {app} from '../../app';

let carroTest:any;
const data = {
    placa:'ABC1020',
    marca:'X',
    chassi:'123456x',
    modelo:'Sport',
    renavam:'1234568x',
    ano:2015
}

describe('Test API de Carros', () => {
    it('Should get All Cars', async () => {
		const res = await supertest(app).get('/cars/all');
        const json   = JSON.parse(res?.text || {});
        assert(json?.carros);
        assert.equal(json?.error,false);
		assert.equal(res?.statusCode,200);
	});

    it('Should create a Car', async () => {
		const res = await supertest(app).post('/cars/create').send(data);
        const json   = JSON.parse(res?.text || {});
        assert(json?.carro);
        assert.equal(json?.error,false);
		assert.equal(res?.statusCode,200);
        carroTest = json?.carro;
	});

    it('Should FAIL create a Car by Required Fields empty', async () => {
        const dataFailed = {
            placa:'ABC1020',
            marca:'X',
            chassi:'123456x',
            modelo:'Sport',
        };
		const res = await supertest(app).post('/cars/create').send(dataFailed);
        const json   = JSON.parse(res?.text || {});
        
        assert.equal(json?.error,true);
        assert(json?.erros);
        assert(json?.msg);
	});

    
    it('Should Update a Car By id Given', async () => {
        data.ano = 2020;
		const res = await supertest(app).put(`/cars/${carroTest?.id}`).send(data);
        const json   = JSON.parse(res?.text || {});
        assert(json?.carro);
        carroTest = json?.carro;
        assert.equal(json?.error,false);
		assert.equal(res?.statusCode,200);
	});

    it('Should Get a Car By id Given', async () => {
		const res = await supertest(app).get(`/cars/find/${carroTest?.id}`).send(data);
        const json   = JSON.parse(res?.text || {});
        assert(json?.carro);
        assert.equal(json?.error,false);
		assert.equal(res?.statusCode,200);
        carroTest = json?.carro;
	});

    it('Should Delete a Car By Id Given', async () => {
		const res = await supertest(app).delete(`/cars/${carroTest?.id}`)
        const json   = JSON.parse(res?.text || {});
        assert.equal(json?.error,false);
		assert.equal(res?.statusCode,200);
        carroTest = null;
	});
})

