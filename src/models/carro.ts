/**
 * Modelo do banco de dados para salvar as informações do carro
 */

 const {Model,DataTypes} = require('sequelize');

 export default class Carro extends Model{
     static init(sequelize){
         super.init({
             createdAt: {
                 field: 'created_at',
                 type: DataTypes.DATE,
             },
             updatedAt: {
                 type: DataTypes.DATE,
                 field: 'updated_at',
             },
             placa: {
                 type: DataTypes.STRING,
                 field: 'placa',
             },
             chassi: {
                type: DataTypes.STRING,
                field: 'chassi',
            },
            marca: {
                type: DataTypes.STRING,
                field: 'marca',
            },
             modelo: {
                type: DataTypes.STRING,
                field: 'modelo',
            },
             renavam: {
                type: DataTypes.STRING,
                field: 'renavam',
            },
             ano: {
                 type: DataTypes.INTEGER,
                 field: 'ano',
             },
         },
         {
             sequelize,
             freezeTableName: true,
             tableName: 'carros',
         })
     }
 
    
 }
 
 export const carroDb = Carro;