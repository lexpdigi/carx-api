/**
 * Controller de Carro responsável pelas por
 * responser as resquisições HTTP
 */


import CarroService from "../modules/carro/carro.service";
const CarroUtil  = require('../utils/car.utils')
import {schemaCarro} from '../validations/carro.validation'
import * as Yup from 'yup'


  export default class CarroController {
    constructor(private readonly carroService: CarroService) {}

    teste(req,res){
        return res.status(200).json(
            {
                'msg':'Rota Carro Works!'
            }
        ) 
    }
  
    async create(req,res) {
      let{data} = req.body
      let dataObj = ( data!=undefined && Object.keys(data).length > 0)?data:req.body
      try{
        await  schemaCarro.validate(dataObj,{abortEarly:false});
        try{
          let  carro = await this.carroService.create(dataObj);
      
          return res.status(200).json({carro:carro,error:false});    
      }catch(err){
        return res.status(200).json({'msg':`Erro ao criar carro: ${err.message}`,error:true});
      }
      }catch(err){
        let errorMessages = {};

        if(err instanceof Yup.ValidationError){
          err.inner.forEach((error) => {
            errorMessages[error.path] = error.message;
          })
        }
        return res.status(400).json({msg:'Dados inválidos',error:true,erros:errorMessages});
      }

    }
  
    async read(req,res) {
      let carros = await this.carroService.findAll();

      return res.status(200).json({carros:carros,error:false});
    }
  
    async findOne(req,res) {

      const {id} = req.params;
      let carro = await this.carroService.findOne(id);

      if(!carro){
        return res.status(404).json({'msg':'Carro Não encontrado!',error:true});
      }


      return res.status(200).json({carro,error:false});
    }
  
    async update(req,res) {
      const {id} = req.params;
      let carro = await this.carroService.findOne(id);

      if(!carro){
        return res.status(404).json({'msg':'Carro Não encontrado!',error:true});
      }

      let{data} = req.body

      let dataObj = ( data!=undefined && Object.keys(data).length > 0)?data:req.body

      try{
        await  schemaCarro.validate(dataObj,{abortEarly:false});
        try{
        await this.carroService.update(id,dataObj);
        return res.status(200).json({'msg':'Carro Atualizado com Sucesso! ',error:false,carro:carro});
      }catch(err){
        return res.status(200).json({'msg':`error ao atualizar carro: ${err.message}`,error:true});
      }
      }catch(err){
        let errorMessages = {};

        if(err instanceof Yup.ValidationError){
          err.inner.forEach((error) => {
            errorMessages[error.path] = error.message;
          })
        }
        return res.status(400).json({msg:'Dados inválidos',error:true,erros:errorMessages});
      }

    }
  
    async delete(req,res) {
      const {id} = req.params;
      let carro = await this.carroService.findOne(id);

      if(!carro){
        return res.status(404).json({'msg':'Carro Não encontrado!',error:true});
      }

      try{
        await this.carroService.delete(id);
        return res.status(200).json({'msg':'carro deletado Com Sucesso!',error:false});
      }catch(err){
        return res.status(200).json({'msg':`Erro ao deletar carro: ${err.message}`,error:true});
      }
    }
  }