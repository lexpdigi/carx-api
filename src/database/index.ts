
/**
 * Arquivo onde starta a conexao do banco de Dados
 */
 const Sequelize = require('sequelize');
import{DbConfig} from '../config/database' 
 import {carroDb} from '../models/carro'
 
 const connection = new Sequelize(DbConfig);
 
 carroDb.init(connection);
 

 module.exports = connection;
 